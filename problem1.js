const carDetailsUsingId = (inventory = [],id = undefined) => {
    let carID = []
    if(inventory.length === 0 || id === undefined ){

        return [];
    }

    for (let i = 0; i < inventory.length; i++) {
        carID = inventory[i];
        
        if(carID.id === id){
            // if car id matches
            //console.log(carID)
            return carID;
        }
    }
    return [];
}
module.exports = carDetailsUsingId
