const bMWandAudiCars = require ("../problem6.js");
const inventory = require("../cars.js");

// cars based on car_year
const carsList = bMWandAudiCars(inventory);

// cars list in Json
const stringifyCarLists = JSON.stringify(carsList);

console.log(stringifyCarLists);
