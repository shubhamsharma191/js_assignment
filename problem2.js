 
const lastCarInInventory = (inventory = []) => {

    if (inventory.length === 0){
       // console.log("check")
        return [];
    }

    // index of last car
    const indOfLastCar = inventory.length -1
    // details of last car
    const lastCar = inventory[indOfLastCar]

    return lastCar;
    }
module.exports = lastCarInInventory