 
const sortingCarModelNames = (inventory = []) => {
    let car;
    let carList = [];
    if (inventory.length === 0){
        return []
    }

    // car models in an array 

    for (let i = 0;i < inventory.length; i++) {
        car = inventory[i];
        carList.push(car.car_model)
    }

    // sorting Car model names alphabetically

    let sortedCar = carList.sort();
    

    return sortedCar


}


module.exports = sortingCarModelNames
